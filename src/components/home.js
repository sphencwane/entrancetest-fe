import React, { Component } from "react";

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.titleEl = React.createRef();
        this.descriptionEl = React.createRef();
        this.priceEl = React.createRef();
        this.dateEl = React.createRef();
    }

    submitHandler = event => {
        event.preventDefault();
        const title = this.titleEl.current.value;
        const description = this.descriptionEl.current.value;
        const price = this.priceEl.current.value;
        const date = this.dateEl.current.value;
        let token = JSON.parse(localStorage.getItem('login'));

        const  requestBody = {
                query: `
                    mutation {
                        createEvent(eventInput:{title:"${title}", description: "${description}",price:${price}, date: "${date}"}){
                            title
                            description
                        }
                    }                    
                `
        };
       
        fetch('http://localhost:8000/api',{
            method: "post",
            body: JSON.stringify(requestBody),
            headers: {
                'Authorization': 'bearer ' +token.token,
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201){
                throw new Error('Failed');
            }
            return res.json();
        })
        .then(resData => {
            if(resData.createEvent.title){
                alert("Record "+ resData.createEvent.title +" Saved!");
            }
        })
        .catch(err => {
            console.log(err);
        });
    };

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                <h3>Create Record</h3>

                <div className="form-group">
                    <label>Title</label>
                    <input type="text" className="form-control" ref={this.titleEl} placeholder="Title" />
                </div>

                <div className="form-group">
                    <label>Description</label>
                    <input type="text" className="form-control" ref={this.descriptionEl} placeholder="Description" />
                </div>

                <div className="form-group">
                    <label>Price</label>
                    <input type="text" className="form-control" ref={this.priceEl} placeholder="Price" />
                </div>

                <div className="form-group">
                    <label>Date</label>
                    <input type="date" className="form-control" ref={this.dateEl} placeholder="Date" />
                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block">Save</button>
            </form>
        );
    }
}