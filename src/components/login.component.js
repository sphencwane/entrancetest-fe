import React, { Component } from "react";
import AuthContext from '../auth-context/auth-context';

import { useHistory } from 'react-router-dom';

export default class Login extends Component {
    state = {
        isLogin : true
    };

    constructor(props) {
        super(props);
        this.emailEl = React.createRef();
        this.passwordEl = React.createRef();
    }

    switchModeHandiler = () => {
        this.setState(prevState => {
            return {isLogin: !prevState.isLogin};
        })
    }

    submitHandler = event => {
        event.preventDefault();
        const email = this.emailEl.current.value;
        const password = this.passwordEl.current.value;

        let requestBody = {
            query: `
                query {
                    login(email:"${email}", password: "${password}"){
                        userId
                        token
                        tokenExpiration
                    }
                }
            `
        };

        if(!this.state.isLogin){
           requestBody = {
                query: `
                    mutation {
                        createUser(userInput:{email:"${email}", password: "${password}"}) {
                            _id
                            email
                        }
                    }                    
                `
            };
        }

        fetch('http://localhost:8000/api',{
            method: "post",
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201){
                throw new Error('Failed');
            }
            return res.json();
        })
        .then(resData => {
            if(resData.data.login.token){
                localStorage.setItem('login',JSON.stringify(resData.data.login));
                this.props.history.push('/home');
            }
        })
        .catch(err => {
            console.log(err);
        });
    };

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                <h3>Log in</h3>
                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" ref={this.emailEl} placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" ref={this.passwordEl} placeholder="Enter password" />
                </div>
                <div className="form-group">
                    <div className="row">
                        <div className="col-md-6">
                            <button type="submit" className="btn btn-primary  btn-block">Submit</button>
                        </div>
                        <div className="col-md-6">
                            <button type="submit" onClick={this.switchModeHandiler} className="btn btn-dark  btn-block">Switch to {this.state.isLogin ? 'Signup' : 'Login'}</button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}